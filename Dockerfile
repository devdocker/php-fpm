FROM ubuntu:14.04.1

MAINTAINER devdocker <www.devdocker.com>

# Install PHP7
RUN apt-get install -y language-pack-en-base && \
    export LC_ALL=en_US.UTF-8 && \
    export LANG=en_US.UTF-8 && \
    apt-get install -y software-properties-common && \
    add-apt-repository -y ppa:ondrej/php

RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y --force-yes supervisor git python \
    gcc make g++ wget vim nginx curl chrpath \
    php7.0 \
    php7.0-cli \
    php7.0-fpm \
    php7.0-mysql \
    php7.0-json \
    php7.0-dev \
    php7.0-odbc \
    php7.0-opcache \
    php7.0-xml \
    php7.0-xsl \
    php7.0-gd \
    php7.0-zip \
    php7.0-curl \
    php7.0-dev




# install dependencies
RUN apt-get clean

# depot tools
RUN git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git /usr/local/depot_tools
ENV PATH $PATH:/usr/local/depot_tools

# install v8
RUN cd /usr/local/src && fetch v8 && \
    cd /usr/local/src/v8 && git checkout 4.9.111 && gclient sync && make x64.release library=shared snapshot=off -j4 && \
    mkdir -p /usr/local/lib && \
    cp /usr/local/src/v8/out/x64.release/lib.target/lib*.so /usr/local/lib && \
    echo "create /usr/local/lib/libv8_libplatform.a\naddlib /usr/local/src/v8/out/x64.release/obj.target/tools/gyp/libv8_libplatform.a\nsave\nend" | ar -M && \
    cp -R /usr/local/src/v8/include /usr/local && \
    chrpath -r '$ORIGIN' /usr/local/lib/libv8.so && \
    rm -fR /usr/local/src/v8

# get v8js, compile and install
ENV NO_INTERACTION 1
RUN git clone https://github.com/preillyme/v8js.git /usr/local/src/v8js && \
    cd /usr/local/src/v8js && phpize && ./configure --with-v8js=/usr/local && \
    make all test install && \
    echo extension=v8js.so > /etc/php/7.0/cli/conf.d/99-v8js.ini && \
    echo extension=v8js.so > /etc/php/7.0/fpm/conf.d/99-v8js.ini && \
    chmod 0777 /etc/php/7.0/fpm/conf.d/99-v8js.ini && \
    chmod 0777 /etc/php/7.0/cli/conf.d/99-v8js.ini && \
    rm -fR /usr/local/src/v8js
    # service nginx reload



#RUN service php7.0-fpm stop && \
 #   service nginx stop && \
  #  service supervisor stop

#ADD conf/supervisord.conf /etc/supervisord.conf
#ADD conf/nginx/sites-enabled/default /etc/nginx/sites-enabled/default
#ADD conf/www.conf /etc/php/7.0/fpm/pool.d/www.conf

#COPY start.sh /start.sh
#RUN chmod +x /start.sh
RUN mkdir -p /run/php /var/run/php
RUN mkdir -p /var/www
ENV TERM xterm-256color

#RUN echo "daemon off;" >> /etc/nginx/nginx.conf
#RUN sed -i 's/sendfile on/sendfile off/' /etc/nginx/nginx.conf
#RUN sed -i 's/user www-data/user root root/' /etc/nginx/nginx.conf

# Install Composer
RUN wget -O /tmp/composer.phar https://getcomposer.org/composer.phar && cp /tmp/composer.phar /usr/local/bin/composer && chmod +x /usr/local/bin/composer

#add default page for testing nginx
#ADD var/www /var/www

#EXPOSE 80

#ENTRYPOINT ["/start.sh"]